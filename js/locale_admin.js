/**
 * collects information for a specific translation string,
 * and executes AJAX call to save it to a persistent file.
 */
function click_handler(obj) {
  var parent_holder = jQuery(obj).parent().parent();
  if(parent_holder.length > 0) {
    var input_els = jQuery('td textarea.first-column', jQuery(parent_holder));
    var output_els = jQuery('td textarea.second-column', jQuery(parent_holder));
    var selected_lang = jQuery('#edit-language-selection').val();
    var selected_type = jQuery('#edit-language-file-selection').val();
    var msgname = jQuery('input.msgname', parent_holder).val();
    var input_list = new Array();
    var output_list = new Array();

    jQuery.each(input_els, function(key, val){
      input_list.push(val.value);
    });
    jQuery.each(output_els, function(key, val){
      output_list.push(val.value);
    });
    //console.log(input_list);
    //console.log(output_list);
    jQuery.ajax({
      type: 'POST',
      url: Drupal.settings.basePath + 'admin/config/regional/advanced_translate/modify',
      dataType: 'json',
      data: {
        msgid: input_list,
        msgstr: output_list,
        sel_lang: selected_lang,
        sel_type: selected_type,
        msgname: msgname
      },
      success: function(response){
        if(response.result) {
          jQuery('p.ajax-callback-result', parent_holder).addClass('success');
        }
        else {
          jQuery('p.ajax-callback-result', parent_holder).addClass('error');
        }
        jQuery('p.ajax-callback-result', parent_holder).html(response.msg);
      },
      error: function(xhr, status, error) {
        alert('Unknown error occured. Error is ' + xhr.responseText);
      }
    });
  }
}
