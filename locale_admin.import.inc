<?php

/**
 * For a specific language, creates a list of files from which we will import language translation sets.
 *
 * @param string $lang
 *        Contains language reference so we know which variables from db we need to get.
 * @return array
 *        Returns a list of full file paths with text group names as indexes.
 */
function _locale_admin_get_file_list($lang = 'en') {
  $lang_list = language_list('enabled');
  $languages = reset($lang_list);
  $docroot   = $_SERVER['DOCUMENT_ROOT'] . '/';
  $files = array();

  foreach (i18n_string_group_info() as $name => $info) {
    $field_name = 'lang_' . $lang . '_' . $name;
    $filepath = variable_get('_local_admin_lang_' . $lang . '_' . $name, NULL);
    if(!empty($filepath)) {
      $files[$name] = $docroot . $filepath;
    }
  }

  $filepath = variable_get('_local_admin_lang_' . $lang . '_builtin', NULL);
  if(!empty($filepath)) {
    $files['default'] = $docroot . $filepath;
  }

  return $files;
}

/**
 * Perform import of all translation text groups (if they exist).
 *
 * This function is mainly called from "admin/config/regional/advanced_translate/settings",
 * but it can also be used from your code.
 *
 * @param string $lang
 *        Language group in which we are importing strings.
 * @param bool $overwrite
 *        Whether should we import and overwrite existing translation strings or not.
 */
function _locale_admin_import_translation($lang = 'en', $overwrite = FALSE) {
  $list = _locale_admin_get_file_list($lang);

  if(!empty($list)) {
    foreach($list as $group => $lang_file) {
      // Ensure we have the file uploaded
      $filename = basename($lang_file);
      $file = new stdClass;
      $file->uid = 1;
      $file->uri = 'public://'.$filename;
      $file->filename = $filename;
      $file->filemime = file_get_mimetype($lang_file);
      $file->status = 1;

      if(file_unmanaged_copy($lang_file, 'public://'.$filename)) {
        $file = file_copy($file, 'private://'.$filename, FILE_EXISTS_REPLACE);
        if(!empty($file)) {
          // Add language, if not yet supported
          drupal_static_reset('language_list');
          $languages = language_list('language');
          $langcode = $lang;

          if (!isset($languages[$langcode])) {
            include_once DRUPAL_ROOT . '/includes/iso.inc';
            $predefined = _locale_get_predefined_list();
            locale_add_language($langcode);
            drupal_set_message(t('The language %language has been created.', array('%language' => t($predefined[$langcode][0]))));
          }

          // Now import strings into the language
          if ($return = _locale_import_po($file, $langcode, $overwrite, $group) == FALSE) {
            $variables = array('%filename' => $file->filename);
            drupal_set_message(t('The translation import of %filename failed.', $variables), 'error');
            watchdog('locale', 'The translation import of %filename failed.', $variables, WATCHDOG_ERROR);
          }
        }
        else {
          drupal_set_message(t('File to import not found.'), 'error');
        }
      }
      else {
        drupal_set_message(t('File %f not imported. Error occured while loading file.', array(
          '%f' => $lang_file
        )), 'error');
      }
    }
  }
  else {
    drupal_set_message(t('There were no files available for import. Import failed.'));
  }
}
