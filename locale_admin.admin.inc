<?php

/**
 * Translation page.
 *
 * @param $form
 *        An associative array containing the structure of the form.
 * @param $form_state
 *        A keyed array containing the current state of the form.
 */
function locale_admin_admin_page($form, &$form_state) {
  global $pager_page_array, $pager_total, $pager_total_items;

  // Reset page counter.
  if(empty($form_state['values']) && empty($form['input'])) {
    $_SESSION['current_page'] = 0;
  }

  $form         = array();
  $lang_list    = language_list('enabled');
  $languages    = reset($lang_list);
  $lang_options = array();

  foreach($languages as $lang) {
    $lang_options[$lang->language] = $lang->name;
  }
  $form['fieldset_selection'] = array(
    '#type' => 'fieldset',
    '#title' => t('Select file for translation'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['fieldset_selection']['lang_hidden'] = array(
    '#type' => 'hidden',
    '#value' => (!empty($lang_type) && !empty($lang_file)) ? '123' : ''
  );
  $form['fieldset_selection']['language_selection'] = array(
    '#type' => 'select',
    '#title' => t('Select language'),
    '#options' => $lang_options,
    '#default_value' => !empty($form_state['input']['language_selection']) ? $form_state['input']['language_selection'] : '',
    '#description' => t('Select which language you wish to work on.'),
  );

  $lang_groups = array(
    'builtin' => 'Built in'
  );
  foreach (i18n_string_group_info() as $name => $info) {
    $lang_groups[$name] = $info['title'];
  }
  $form['fieldset_selection']['language_file_selection'] = array(
    '#type' => 'select',
    '#title' => t('Select language group'),
    '#options' => $lang_groups,
    '#default_value' => !empty($form_state['input']['language_selection']) ? $form_state['input']['language_selection'] : '',
    '#description' => t('Select which language file to load.'),
  );
  $form['fieldset_selection']['language_file_button'] = array(
    '#type' => 'button',
    '#value' => t('Load'),
    '#ajax' => array(
      'callback' => 'locale_admin_admin_page_ajax_load_table_callback',
      'wrapper' => 'table-wrapper-div',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  $form['table_wrapper_start'] = array(
    '#prefix' => '<div id="table-wrapper-div">'
  );
  $form['current_page'] = array(
    '#type' => 'hidden',
    '#value' => 1
  );
  $form['table_wrapper_end'] = array(
    '#prefix' => '</div>'
  );
  $form['load_more_btn'] = array(
    '#type' => 'button',
    '#value' => t('Load more'),
    '#attributes' => array(
      'style' => 'display: none'
    ),
    '#ajax' => array(
      'callback' => 'locale_admin_admin_page_ajax_load_table_callback',
      'wrapper' => 'table-wrapper-div',
      'method' => 'append',
      'effect' => 'fade',
    ),
  );

  drupal_add_js(drupal_get_path('module', 'locale_admin') . '/js/locale_admin.js');
  drupal_add_css(drupal_get_path('module', 'locale_admin') . '/css/locale_admin.css');

  return $form;
}

/**
 * Drupal Form API Ajax callback to load translation strings.
 *
 * @param $form
 *        An associative array containing the structure of the form.
 * @param $form_state
 *        A keyed array containing the current state of the form.
 * @return array
 *        Return partial form populated with new data.
 */
function locale_admin_admin_page_ajax_load_table_callback($form, &$form_state) {
  $lang_list    = language_list('enabled');
  $languages    = reset($lang_list);

  $lang_type = !empty($form_state['values']['language_selection']) ? $form_state['values']['language_selection'] : '';
  $lang_file = !empty($form_state['values']['language_file_selection']) ? $form_state['values']['language_file_selection'] : '';

  if(!empty($lang_type) && !empty($lang_file)) {
    $varname  = '_local_admin_lang_' . $lang_type . '_' . $lang_file;
    $file     = variable_get($varname, '');
    $fullpath = $_SERVER['DOCUMENT_ROOT'] . '/' . $file;
    if(is_file($file) && is_readable($fullpath)) {
      module_load_include('php', 'locale_admin', 'includes/PoParser');
      $page       = !empty($_SESSION['current_page']) ? $_SESSION['current_page'] : 0;
      $offset     = variable_get('_locale_admin_num_results_per_page', 30);
      $start      = $page * $offset;
      $poparser   = new Sepia\PoParser();
      $entries    = $poparser->read($fullpath);

      $header = array();
      foreach($languages as $lang) {
        $header[] = array(
          'data' => t('Language string for %lang language', array(
            '%lang' => $lang->name
          ))
        );
      }
      $header[] = array(
        'data' => t('Options')
      );

      $rows = array();
      $counter = 0;
      foreach($entries as $entry_name => $entry) {
        if($counter >= $start && $counter < ($offset +$start)) {
          $lang_1   = array();
          $count_1  = count($entry['msgid']);
          $lang_2   = array();
          $count_2  = count($entry['msgstr']);

          foreach($entry['msgid'] as $index => $msgid) {
            $lang_1['input_lang_' . $index] = array(
              '#type'       => 'textarea',
              '#title'      => t(''),
              '#attributes' => array(
                'data-id'   => $msgid,
                'readonly'  => 'readonly',
                'class'     => array('first-column')
              ),
              '#cols' => 40,
              '#rows' => 2,
              '#default_value' => $msgid,
              '#value' => $msgid
            );
          }
          if($count_1 < $count_2) {
            for($i=$count_1; $i<$count_2; $i++) {
              $lang_1['input_lang_' . $i] = array(
                '#type'       => 'textarea',
                '#title'      => t(''),
                '#attributes' => array(
                  'data-id'   => '',
                  'readonly'  => 'readonly',
                  'class'     => array('first-column')
                ),
                '#cols' => 40,
                '#rows' => 2,
                '#default_value' => '',
                '#value' => ''
              );
            }
          }



          foreach($entry['msgstr'] as $index => $msgstr) {
            $lang_2['output_lang_' . $index] = array(
              '#type'       => 'textarea',
              '#title'      => t(''),
              '#attributes' => array(
                'data-id'   => $msgstr,
                'class'     => array('second-column')
              ),
              '#cols' => 40,
              '#rows' => 2,
              '#default_value' => $msgstr,
              '#value' => $msgstr
            );
          }
          // If theres no translation, add them.
          if($count_2 < $count_1) {
            for($i=$count_2; $i<$count_1; $i++) {
              $lang_2['output_lang_' . $i] = array(
                '#type'       => 'textarea',
                '#title'      => t(''),
                '#attributes' => array(
                  'data-id'   => $msgstr,
                  'class'     => array('second-column')
                ),
                '#cols' => 40,
                '#rows' => 2,
                '#default_value' => '',
                '#value' => ''
              );
            }
          }
          $hidden = array(
            '#type' => 'hidden',
            '#attributes' => array(
              'class' => array('msgname')
            ),
            '#value' => $entry_name
          );
          $row [] = array(
            array(
              'data' => drupal_render($lang_1)
            ),
            array(
              'data' => drupal_render($lang_2)
            ),
            array(
              'data' => '<input type="button" name="save-translation-'.$counter.'" class="save-translation-btn form-submit" ' .
                        'id="save-translation-'.$counter.'" onClick="javascript:click_handler(this);" value="' . t('Save translation') . '" />' .
                        '<p class="ajax-callback-result"></p>' .
                        drupal_render($hidden)
            )
          );
        }
        $counter++;
      }

      // This is where we create the table using theme()
      $output = theme('table', array(
        'header' => $header,
        'rows' => $row
      ));

      $form['wrapper_start'] = array(
        '#markup' => '<div id="table-wrapper-div">' .
          '<script>' .
          "jQuery(document).ready(function(){ jQuery('input#edit-load-more-btn').removeAttr('style'); });" .
          '</script>'
      );

      $form['table'] = array(
        '#markup' => $output
      );

      $form['current_page'] = array(
        '#type' => 'hidden',
        '#attributes' => array(
          'name' => 'current_page'
        ),
        '#value' => ++$page
      );
      $_SESSION['current_page']++;
      $form['wrapper_end'] = array(
        '#markup' => '</div>'
      );
      $form_state['rebuild'] = TRUE;

      return array(
        $form['wrapper_start'], $form['table'], $form['current_page'], $form['wrapper_end']
      );
    }
    else {
      return array(
        '#markup' => '<div id="table-wrapper-div"><p>' . t('File not available or corrupt.') .'</p></div>'
      );
    }
  }
}

/**
 * Settings page.
 *
 * @param $form
 *        An associative array containing the structure of the form.
 * @param $form_state
 *        A keyed array containing the current state of the form.
 * @return array
 *        Returns complete form definition.
 */
function locale_admin_admin_settings_page($form, $form_state) {
  $form = array();

  $lang_list = language_list('enabled');
  $languages = reset($lang_list);

  $form['fieldset_num_results'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['fieldset_num_results']['num_results'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Number of results per page'),
    '#default_value'  => variable_get('_locale_admin_num_results_per_page', 30),
    '#size'           => 10,
    '#maxlength'      => 10,
    '#required'       => TRUE,
    '#description'    => t('Define how many translation strings will show up per page.')
  );

  $form['fieldset_actions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Available actions'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $lang_opts = array();
  foreach($languages as $lang) {
    $lang_opts[$lang->language] = $lang->name;
  }
  $form['fieldset_actions']['lang_opts'] = array(
    '#type' => 'select',
    '#title' => t('Select language'),
    '#options' => $lang_opts,
    '#description' => t('Select language for which you wish to execute action.'),
  );
  $form['fieldset_actions']['mode'] = array(
    '#type' => 'select',
    '#title' => t('Select method'),
    '#options' => array(
      '0' => t('Import everything, overwrite existing.'),
      '1' => t('No overwrites, only new items are imported.'),
    ),
    '#description' => t('Select language for which you wish to execute action.'),
  );
  $form['fieldset_actions']['apply'] = array(
    '#type' => 'submit',
    '#value' => t('Apply translations'),
    '#description' => t('Imports translation string from exported files, but imports translation only for non-existent translations.')
  );

  foreach($languages as $lang) {
    $form['fieldset_lang_' . $lang->language] = array(
      '#type' => 'fieldset',
      '#title' => t('Translation settings for %lang language', array(
        '%lang' => $lang->name
      )),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('Enter path relative to docroot. Include final slash.')
    );

    foreach (i18n_string_group_info() as $name => $info) {
      $form['fieldset_lang_' . $lang->language]['lang_' . $lang->language . '_' . $name] = array(
        '#type' => 'textfield',
        '#title' => t('Path to %type file', array(
          '%type' => $name
        )),
        '#size' => 120,
        '#default_value' => variable_get('_local_admin_lang_' . $lang->language . '_' . $name, ''),
        '#description' => t('Configure path to translated po file that contains strings in %lang language.', array(
          '%lang' => $lang->name
        )),
      );
    }

    $form['fieldset_lang_' . $lang->language]['lang_' . $lang->language . '_builtin'] = array(
      '#type' => 'textfield',
      '#title' => t('Path to %type file', array(
        '%type' => 'builtin'
      )),
      '#size' => 120,
      '#default_value' => variable_get('_local_admin_lang_' . $lang->language . '_builtin', ''),
      '#description' => t('Configure path to translated po file that contains strings in %lang language.', array(
        '%lang' => $lang->name
      )),
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );

  return $form;
}

/**
 * Validation for Settings page.
 *
 * @param $form
 *        An associative array containing the structure of the form.
 * @param $form_state
 *        A keyed array containing the current state of the form.
 */
function locale_admin_admin_settings_page_validate($form, &$form_state) {
  if($form_state['triggering_element']['#value'] == t('Save')) {
    $lang_list = language_list('enabled');
    $languages = reset($lang_list);
    $docroot   = $_SERVER['DOCUMENT_ROOT'] . '/';
    foreach($languages as $lang) {
      foreach (i18n_string_group_info() as $name => $info) {
        $field_name = 'lang_' . $lang->language . '_' . $name;
        $filepath = $form_state['values'][$field_name];
        $full_path = $docroot . $filepath;
        if(!file_exists($full_path) && !is_readable($full_path)) {
          form_set_error($name, t('File %filepath does not exists or is not readable.', array(
            '%filepath' => $filepath
          )));
        }
      }
    }
  }
}

/**
 * Submit handler for the Settings page.
 *
 * @param $form
 *        An associative array containing the structure of the form.
 * @param $form_state
 *        A keyed array containing the current state of the form.
 */
function locale_admin_admin_settings_page_submit($form, &$form_state) {
  $lang_list = language_list('enabled');
  $languages = reset($lang_list);
  $docroot   = $_SERVER['DOCUMENT_ROOT'] . '/';

  if($form_state['triggering_element']['#id'] == 'edit-submit') {
    $files     = array();
    foreach($languages as $lang) {
      foreach (i18n_string_group_info() as $name => $info) {
        $field_name = 'lang_' . $lang->language . '_' . $name;
        $filepath = $form_state['values'][$field_name];
        if(!empty($filepath)) {
          $full_path = $docroot . $filepath;
          $files[$lang->language][$name] = $filepath;
          variable_set('_local_admin_lang_' . $lang->language . '_' . $name, $filepath);
        }
      }
      $field_name = 'lang_' . $lang->language . '_builtin';
      $filepath = $form_state['values'][$field_name];
      if(!empty($filepath)) {
        $full_path = $docroot . $filepath;
        $files[$lang->language]['builtin'] = $filepath;
        variable_set('_local_admin_lang_' . $lang->language . '_builtin', $filepath);
      }
    }

    variable_set('_locale_admin_num_results_per_page', $form_state['values']['num_result']);
    drupal_set_message(t('Your settings have been saved'));
  }
  elseif($form_state['triggering_element']['#id'] == 'edit-apply') {
    module_load_include('inc', 'locale_admin', 'locale_admin.import');
    $result = _locale_admin_import_translation($form_state['values']['lang_opts'], $form_state['values']['mode']);
  }
}
