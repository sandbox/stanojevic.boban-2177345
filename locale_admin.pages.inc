<?php

/**
 * AJAX callback that saves translation string to persistent file.
 *
 * Uses PoParse class.
 * @see https://github.com/raulferras/PHP-po-parser
 */
function locale_admin_modify_ajax_callback() {
  $lang_type  = $_POST['sel_lang'];
  $lang_file  = $_POST['sel_type'];
  $msgname    = $_POST['msgname'];
  $msgid      = $_POST['msgid'];
  $msgstr     = $_POST['msgstr'];
  $varname    = '_local_admin_lang_' . $lang_type . '_' . $lang_file;
  $file       = variable_get($varname, '');
  $fullpath   = $_SERVER['DOCUMENT_ROOT'] . '/' . $file;
  $result     = array();

  if(is_file($fullpath) && is_writeable($fullpath)) {
    module_load_include('php', 'locale_admin', 'includes/PoParser');

    $poparser   = new Sepia\PoParser();
    $entries    = $poparser->read($fullpath);
    $poparser->update_entry($msgname, $msgstr);
    $poparser->write($fullpath);
    $result     = array(
      'result' => true,
      'msg'    => t('Translation saved successfully.')
    );
  }
  else {
    $result     = array(
      'result' => false,
      'msg'    => t('Error occured while saving translation.')
    );
  }

  $result['input'] = $_POST;
  $result['file'] = $fullpath;
  drupal_json_output($result);
}
